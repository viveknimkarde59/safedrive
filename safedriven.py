
#Introduction:This Module aims at applying machinelearning to the vehicle data . 


#Data is loaded for processing from mongodb database
import pymongo
#pymongo is the python package used for loading data from mongodb 

 

pymongo


# In[6]:

from pymongo import MongoClient


# In[7]:The data is loaded from local mongodb database

client = MongoClient('mongodb://127.0.0.1:3001/meteor')


# In[8]:select Database

db = client.meteor



# In[9]: select collection

collection = db.tasks
#print(collection)


# In[10]:convert all entries from database to list datastructure

data = list(collection.find())
#numpy is the library used for numerical calculation
import numpy as np 
da = np.array(data)
#print(da)


#This part performs datawrangling and makes it available in required format for processing 


y_acc = np.array([i['y_acc'] for i in da]).astype(np.float)
#print(y_acc.mean())


# In[12]:

x_acc = np.array([i['x_acc'] for i in da]).astype(np.float)
#print(x_acc.mean())
z_acc = np.array([i['z_acc'] for i in da]).astype(np.float)
#print(z_acc.mean())


# In[13]: Calculate the total acceleration from acceleration of x,y,z axis.

acc_total_squares =np.square(x_acc)+np.square(y_acc)+np.square(z_acc)
#print(acc_total_squares)


# In[88]:

acc_total = np.sqrt(acc_total_squares)
acc_class = []
# divide accelerations in diiferent classes
for i in acc_total:
    
    if i <=0.2 :
         acc_class.append(1)
    elif 1< i  :
         acc_class.append(3)
    else :
        acc_class.append(0)
        
    
    
#print(acc_class)


# In[89]:
#perform data wrangling on lattitude and longitude
lattitude = np.array([i['latitude'] for i in da]).astype(np.float)
longitude = np.array([i['longitude'] for i in da]).astype(np.float)
for i in range(len(lattitude)):
    lattitude[i] = lattitude[i]+i;
for i in range(len(longitude)):
    longitude[i] = longitude[i]+i;
    



# In[90]:

#Transform the data in the required format used by sklearn by using zip function.
#[d1,d2] format
x = [[float(round(gg,4))  , float(round(ss,4))] for gg, ss in zip( lattitude , longitude)]


# In[91]:

#print(x)


# In[ ]:

import numpy as np
# Use Scikit  learn library for building machine learning model 

from sklearn.naive_bayes import GaussianNB
#import svm(Support Vector Machine) 
from sklearn import svm
#classify function takes training features and labels returns trained model
def classify(features_train, labels_train):   
    ### import the sklearn module for GaussianNB
    ### create classifier
    ### fit the classifier on the training features and labels
    ### return the fit classifier


    ### your code goes here!
    clf = svm.SVC(kernel='rbf',gamma=0.1)
    clf.fit(features_train,labels_train)
    return clf


# In[101]:

import seaborn as sns
#plotting library matplotlib
import pylab as pl
import matplotlib.pyplot as plt
#clf is the trained model
clf = classify(x,acc_class)
x_min =10; x_max =150 
y_min = 10; y_max = 150

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
h = 0.5  # step size in the mesh
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
c  = clf.predict([120, 60])
print(c)
    # Put the result into a color plot
Z = Z.reshape(xx.shape)
print(Z)
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())

plt.pcolormesh(xx, yy, Z, cmap=pl.cm.seismic)

plt.legend()
plt.xlabel("bumpiness")
plt.ylabel("grade")

#plt.show()
#This Function is called from FlaskAPI to analyse the perticular journey This function returns 
def analyse_journey():
#some parameters are predefined for testing purpose 
    journey_data_array = list(collection.find({ 'owner':'nt6pWhmhffYRySi3z','time': { '$gte':1484024598700.0 , '$lte': 1484058163950.0 } }))
    journey_data_j = np.array(journey_data_array)
#     print(journey_data_j)
    
    y_acc_j = np.array([i['y_acc'] for i in journey_data_j]).astype(np.float)
 
    x_acc_j = np.array([i['x_acc'] for i in journey_data_j]).astype(np.float)
   
    z_acc_j = np.array([i['z_acc'] for i in journey_data_j]).astype(np.float)
 
    acc_total_squares_j =np.square(x_acc_j)+np.square(y_acc_j)+np.square(z_acc_j)
  
    acc_total_j = np.sqrt(acc_total_squares_j)
    acc_class_j = []
#     print(acc_total_squares)
    for i in acc_total_j:
    
        if i <=0.2 :
             acc_class_j.append(1)
        elif 1< i  :
             acc_class_j.append(3)
        else :
            acc_class_j.append(0)
        
    
    
#     print(acc_class_j)
    lattitude_j = np.array([i['latitude'] for i in journey_data_j]).astype(np.float)
    longitude_j = np.array([i['longitude'] for i in journey_data_j]).astype(np.float)
    for i in range(len(lattitude_j)):
        lattitude_j[i] = lattitude_j[i]+i;
    for i in range(len(longitude_j)):
        longitude_j[i] = longitude_j[i]+i;

        
    x = [[float(round(gg,4))  , float(round(ss,4))] for gg, ss in zip( lattitude_j , longitude_j)]

    c  = clf.predict(x)
#     print(c)
    diff = []
#     print(len(diff))
#     print(len(c))
#     print(len(acc_total_j))
#This algorithm compares predicted and actual value of accelerations and calculates the rating 
    for i in range(len(acc_total_j)):
        if acc_class_j[i] > c[i] :
            diff.append(acc_class_j[i]-c[i])
#             print(acc_class_j[i],c[i],lattitude_j[i])
    unsafe_mean=np.array(diff).mean()
    print(unsafe_mean)
    return (len(diff)/len(c))*unsafe_mean*100
# In[1]:
from twilio import TwilioRestException
from twilio.rest import TwilioRestClient

account_sid = "ACb2ffbf2dc6fe83fdb97bc3a340d65a5e" # Your Account SID from www.twilio.com/console
auth_token  = "49c43cdd1127266e19165fa65536a5e7"  # Your Auth Token from www.twilio.com/console

client = TwilioRestClient(account_sid, auth_token)
def send_message():
     # message = client.messages.create(body="My Name is Vivek",to="+919420939071",from_="+16265392464") 
      return 5



#Flask API is used to create endpoints to access the app from front end 
from flask_api import FlaskAPI
from flask_cors import CORS
from flask import request
from OpenSSL import SSL
import os
 
context = SSL.Context(SSL.SSLv23_METHOD)
cer = os.path.join(os.path.dirname(__file__), 'server.crt')
key = os.path.join(os.path.dirname(__file__), 'server.key')
 
app = FlaskAPI(__name__)
CORS(app)
@app.route('/getData/')
def getData():
    
 
    return str(analyse_journey())
@app.route('/danger/')
def danger():
    print(request.args['time'])
    print(request.args['user'])
    print(request.args['lat'])
    print(request.args['long'])
    
    return send_message()
#Creates the HTTPS server
if __name__=="__main__":
    context = (cer, key)
    app.run( host='192.168.119.103', debug = True, ssl_context=context)


# In[ ]:



