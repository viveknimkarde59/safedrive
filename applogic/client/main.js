import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngMaterial from 'angular-material';

import uiRouter from 'angular-ui-router';
import safeDrive from '../imports/components/safeDrive/safeDrive';
import '../imports/startup/accounts-config.js';
angular.module('safe-drive', [
    angularMeteor,
    ngMaterial,
    uiRouter,
    safeDrive.name,
    'accounts.ui'
])
;
function onReady() {
  angular.bootstrap(document, ['safe-drive']);
}
 
if (Meteor.isCordova) {
  
  angular.element(document).on('deviceready', onReady);
} else {
  angular.element(document).ready(onReady);
}

