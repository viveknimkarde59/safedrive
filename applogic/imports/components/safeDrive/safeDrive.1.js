import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './safeDrive.html';

import {
  Tasks
} from '../../api/tasks.js';
import {
  Meteor
} from 'meteor/meteor';



function safeDriveCtrl($scope) {

  $scope.viewModel(this);



 
  this.openLeftMenu = function () {
    $mdSidenav('left').toggle();
  }

  
 


}









insuranceCtrl.$inject = ['$scope'];
export default angular.module('safeDrive', [
    angularMeteor,

  ])
  .component('insurancePartner', {
    templateUrl: 'imports/components/safeDrive/insurance.html',
    controller: ['$scope', insuranceCtrl],
    bindings: {
      count: '='
    }
  })

  .config(config);

function config($locationProvider, $urlRouterProvider, $stateProvider) {
  'ngInject';



  $stateProvider
    .state('insurance', {
      url: '/insurance',
      component: 'insurancePartner',
    });
};