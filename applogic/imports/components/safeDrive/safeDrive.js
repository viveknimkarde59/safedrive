import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './safeDrive.html';
import template2 from './insurance.html';
import template1 from './dialog1.tmpl.html';
import {
  Tasks
} from '../../api/tasks.js';
import {
  Meteor
} from 'meteor/meteor';

function DialogController($scope, $mdDialog, rating) {
  $scope.hide = function () {
    $mdDialog.hide();
  };
  $scope.rating = rating;
  console.log(rating);
  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.answer = function (answer) {
    $mdDialog.hide(answer);
  };
}

function safeDriveCtrl($scope, $mdSidenav, $element, $interval, $http, $timeout, $mdDialog, $state) {

  $scope.viewModel(this);
  console.log($element);

  this.x_acc = 0;
  this.y_acc = 0;
  this.z_acc = 0;
  this.a_rot = 0;
  this.b_rot = 0;
  this.g_rot = 0;
  this.lat = 0;
  this.rating = "Loading....";
  this.lang = 0;
  //   this.helpers({

  //         tasks(){
  //             const selector = {};

  //   // If hide completed is checked, filter tasks
  //            if (this.getReactively('hideCompleted')) {
  //                     selector.checked = {
  //                     $ne: true
  //                     };
  //             }
  //            return Tasks.find(selector, {
  //                   sort: {
  //                   createdAt: -1
  //                   }
  //             });
  //          }
  //  });
  this.currentUser = function () {
    return Meteor.userId();
  }


  // Clear form
  this.showAdvanced = function (ev) {
    var _this = this;
    $mdDialog.show({
        controller: DialogController,
        controllerAs: 'ctrl',

        templateUrl: 'imports/components/safeDrive/dialog1.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
        locals: {
          rating: this.rating
        }
      })
      .then(function (answer) {
        console.log(answer);
        if (answer == 'graboffer') {
          $state.go('insurance');
        }

      }, function () {
        $scope.status = 'You cancelled the dialog.';
      });
  };

  this.startJourney = function () {
    var _this = this;
    this.start_time = Math.floor(Date.now());
    // _this.cllect_data = 
    this.start = true;
    this.collect_data = $interval(function () {
      navigator.geolocation.getCurrentPosition(function showPosition(position) {
        _this.lat = position.coords.latitude.toFixed(4);
        _this.lang = position.coords.longitude.toFixed(4);
        _this.time = position.timestamp;
        _this.speed = position.coords.speed;

        Tasks.insert({
          latitude: _this.lat,
          longitude: _this.lang,
          x_acc: _this.x_acc,
          y_acc: _this.y_acc,
          z_acc: _this.z_acc,
          a_rot: _this.a_rot,
          b_rot: _this.b_rot,
          g_rot: _this.g_rot,
          time: _this.time,
          speed: _this.speed,
          owner: Meteor.userId(),
          username: Meteor.user().username,
        });
        console.log(_this.time);

      });
      if (!((Math.sqrt((_this.x_acc * _this.x_acc) + (_this.y_acc * _this.y_acc) + (_this.z_acc * _this.z_acc)) > 1))) {
        //  _this.danger();
      }
    }, 1000);
  }
  //   this.danger = function(){

  // this.danger_time = Math.floor(Date.now());
  //     var _this = this;
  //    _this.alert_msg = $timeout(function () {

  //   console.log(_this);
  //     var req = {
  //  method: 'GET',
  //  url: 'http://127.0.0.1:5000/danger/',
  //  params: {
  //     time : _this.danger_time,
  //     user : Meteor.user().username,
  //     lat : _this.lat,
  //     long : _this.lang,

  //  }
  //  }
  //      $http(req)
  //     .then(function(response) {

  //         _this.danger_message = response.data;
  // console.log(_this.danger_message);
  //     });
  //     }, 500);
  //   }
  this.openLeftMenu = function () {
    $mdSidenav('left').toggle();
  }
  this.setChecked = function (task) {
    // Set the checked property to the opposite of its current value
    Tasks.update(task._id, {
      $set: {
        checked: !task.checked
      },
    });
  }
  this.stop = function () {

    this.start = false;
    this.stop_time = Math.floor(Date.now());

    var _this = this;
    $interval.cancel(_this.collect_data);
    var req = {
      method: 'GET',
      url: 'https://192.168.119.103:5000/getData/',
      params: {
        start_time: _this.start_time,
        stop_time: _this.stop_time,
        user: Meteor.userId(),

      }
    }
    $http(req)
      .then(function (response) {

        _this.rating = response.data;
        console.log(_this.rating);
        _this.showAdvanced();
      });


  }
  this.iamok = function () {
    $timeout.cancel(this.alert_msg);
  }
  this.$postLink = function () {

    var _this = this;
    window.addEventListener('devicemotion', function (event) {

      _this.x_acc = event.acceleration.x.toFixed(4);
      _this.y_acc = event.acceleration.y.toFixed(4);
      _this.z_acc = event.acceleration.z.toFixed(4);
      _this.a_rot = event.rotationRate.alpha.toFixed(4);
      _this.b_rot = event.rotationRate.beta.toFixed(4);
      _this.g_rot = event.rotationRate.gamma.toFixed(4);


      $scope.$digest();

    });



  }
}


function insuranceCtrl($scope) {

  $scope.viewModel(this);




  this.openLeftMenu = function () {
    $mdSidenav('left').toggle();
  }





}








safeDriveCtrl.$inject = ['$scope', '$mdSidenav', '$element', '$interval', '$http', '$timeout', '$mdDialog', '$state'];
export default angular.module('safeDrive', [
    angularMeteor,

  ])
  .component('safeDrive', {
    templateUrl: 'imports/components/safeDrive/safeDrive.html',
    controller: ['$scope', '$mdSidenav', '$element', '$interval', '$http', '$timeout', '$mdDialog', '$state', safeDriveCtrl],
    bindings: {
      count: '='
    }
  }).component('insurancePartner', {
    templateUrl: 'imports/components/safeDrive/insurance.html',
    controller: ['$scope', insuranceCtrl],

  })

  .config(config);

function config($locationProvider, $urlRouterProvider, $stateProvider) {
  'ngInject';
  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/drive');
  $stateProvider
    .state('drive', {
      url: '/drive',
      component: 'safeDrive',
    })
    .state('insurance', {
      url: '/insurance',
      component: 'insurancePartner',
    });
};
